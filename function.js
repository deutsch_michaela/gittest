const XmlFormatterAndValidator = () => {
    const [xmlInput, setXmlInput] = useState('');
    const [validationResult, setValidationResult] = useState('');
    const [copied, setCopied] = useState(false);
  
    const textareaRef = useRef(null);
  
    const formatXml = () => {
      try {
        const formattedXmlString = vkbeautify.xml(xmlInput, 2);
        setXmlInput(formattedXmlString);
        setValidationResult('');
      } catch (error) {
        console.error('Error formatting XML:', error);
        setXmlInput('Invalid XML format');
        setValidationResult('');
      }
    };
}